<?php bb_get_header(); ?>
    <div class="bbcrumb"><a href="<?php bb_uri(); ?>"><?php bb_option('name'); ?></a> / <?php _e('یافت نشد!'); ?></div>  
    <div id="fcontent"> 
        <h2 id="http404">برگه‌ای یافت نشد!</h2>    
        <p>متاسفانه چیزی در این نشانی وجود ندارد. نمی‌خواهید برای آن جست‌وجویی کنید؟</p>
        
        <?php search_form(); ?>
    </div>
<?php bb_get_footer(); ?>