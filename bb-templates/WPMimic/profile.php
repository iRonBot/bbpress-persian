<?php bb_get_header(); ?>
        <div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » شناس‌نامه‌ی <?php echo get_user_name( $user->ID ); ?></div>
        <div id="sidebar"><!-- Sidebar -->
            <ul>
                <li style="border: none; padding: 25px; background: #eee; border: 1px solid #ddd;"><?php echo bb_get_avatar( $user->ID,$size='250'); ?></li>
				 <?php
                 global $bb_table_prefix;
                 $query1 = "SELECT COUNT(*) FROM ".$bb_table_prefix."posts WHERE poster_id = $user_id AND post_status = 0";
                 $query2 = "SELECT COUNT(*) FROM ".$bb_table_prefix."posts WHERE poster_id = $user_id AND post_status = 0 AND post_position = 1";
                 echo "<li>تعداد نوشته‌ها: ".$bbdb->get_var($query1)."</li>";
                 echo "<li>تعداد موضوع‌های آغازشده: ".$bbdb->get_var($query2)."</li>"; ?>              
            </ul>
			<?php if (bb_profile_data() ); ?>
            <?php if (is_bb_profile()) profile_menu(); ?>
        <br></div><!-- /sidebar -->
<div id="ajax">
	<div id="ajaxInner">
        <div id="content"><!-- Content -->
            <table>
                <thead>
                <tr>
                    <th class="title">آخرین پاسخ‌ها</th>
                    <th><?php if ( $user->ID == bb_get_current_user_info( 'id' ) ) printf('آخرین پاسخ شما'); else printf('آخرین پاسخ کاربر'); ?></th>
                    <th>آخرین پاسخ</th>
                </tr>
                </thead>
                <tbody>
                <?php if ( $posts ) : ?>
                <?php foreach ($posts as $bb_post) : $topic = get_topic( $bb_post->topic_id ) ?>
                <tr<?php alt_class('replies'); ?>>
                    <td class="title"><a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                    <td><?php if ( $user->ID == bb_get_current_user_info( 'id' ) ) printf(__('%s پیش'), bb_get_post_time()); else printf(__('%s پیش'), bb_get_post_time()); ?></td>
                    <td><?php if ( bb_get_post_time( 'timestamp' ) < get_topic_time( 'timestamp' ) ) printf(__('%s پیش'), get_topic_time()); else _e('بدون پاسخ');?></td>
                </tr>
				<?php endforeach; ?>
            
				<?php else : if ( $page ) : ?>
                <tr>
                	<td class="title">پاسخ‌های بیشتری داده نشده.</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php else : ?>
                <tr>
                	<td class="title">بدون پاسخ تا به‌حال</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php endif; endif; ?>
                </tbody>
            </table>

            <table>
                <thead>
                <tr>
                    <th class="title">موضوع‌های آغازشده</th>
                    <th>آعازشده</th>
                    <th>آخرین پاسخ</th>
                </tr>
                </thead>
                <tbody>
        		<?php if ( $topics ) : ?>
                <?php foreach ($topics as $topic) : ?>
                <tr<?php alt_class('topics'); ?>>
                    <td class="title"><a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                    <td><?php printf(__('%s پیش'), get_topic_start_time()); ?></td>
                    <td><?php if ( get_topic_start_time( 'timestamp' ) < get_topic_time( 'timestamp' ) ) printf(__('%s پیش'), get_topic_time()); else _e('بدون پاسخ'); ?></td>
                </tr>
				<?php endforeach; ?>
				<?php else : if ( $page ) : ?>
                <tr>
                	<td class="title">موضوع‌های بیشتری آغاز نشده.</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php else : ?>
                <tr>
                	<td class="title">.هیچ موضوعی تا به‌حال آغاز نشده</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php endif; endif; ?>
                </tbody>
            </table>
        </div><!-- /content -->
		<div id="postPagination"><div class="nav"><?php profile_pages(); ?></div></div>
	</div>
</div>
<?php bb_get_footer(); ?>