            <div class="post">
				<div class="post-content resp">
					<?php if ( !bb_is_topic() ) : ?>
					<p>عنوان موضوع:<br><input name="topic" type="text" id="topic" size="50" maxlength="80" tabindex="1" /></p>
					<?php endif; do_action( 'post_form_pre_post' ); ?>
					<p>نوشته:<br><textarea name="post_content" cols="50" rows="8" id="post_content" tabindex="3" ></textarea></p>
					<p>برچسب‌ها (با کامای انگلیسی آن‌ها را از هم جدا کنید):<br><input id="tags-input" name="tags" type="text" size="50" maxlength="100" value="<?php bb_tag_name(); ?>" tabindex="4" /></p>
					
					<?php if ( bb_is_tag() || bb_is_front() ) : ?>
						<p class="new-topic">انجمنی برای موضوع خود انتخاب کنید: <?php bb_new_topic_forum_dropdown(); ?></p>
					<?php endif; ?>
					<div class="chekbox"><?php if ( bb_is_user_logged_in() && bb_is_subscriptions_active() ) : ?>
						<p class="post-form-subscription-container"><?php bb_user_subscribe_checkbox( 'tab=38' ); ?>
					<?php endif; ?>
					<br /><input type="submit" id="postformsub" name="Submit" value="  فرستادن نوشته  " tabindex="4" /></p></div>
				</div>
				<div class="post-user">
					<?php post_author_avatar($size='100'); ?>
					<ul>
						<li><?php post_author_link(); ?></li>
						<li><?php post_author_title(); ?></span></li>
					</ul>
				</div>    
			</div>
