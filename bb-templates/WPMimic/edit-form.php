        	<div class="post">
            	<div class="post-content">
					<?php if ( $topic_title ) : ?>
                    <input name="topic" type="text" id="topic" size="50" maxlength="80"  value="<?php echo esc_attr( get_topic_title() ); ?>" />
                    <?php endif; do_action( 'edit_form_pre_post' ); ?>
                    <textarea name="post_content" cols="50" rows="8" id="post_content"><?php echo apply_filters('edit_text', get_post_text() ); ?></textarea>
                    <?php if ( bb_get_user( get_post_author_id() ) && bb_is_subscriptions_active() ) : ?>
						<p id="post-form-subscription-container" class="left">
							<?php bb_user_subscribe_checkbox( 'tab=33' ); ?>
						</p>
					<?php endif; ?>

                    <input type="submit" name="Submit" value="<?php echo esc_attr__( 'ویرایش نوشته &raquo;' ); ?>" />
                    <input type="hidden" name="post_id" value="<?php post_id(); ?>" />
                    <input type="hidden" name="topic_id" value="<?php topic_id(); ?>" />
                </div>
                
                <div class="post-user">
					<?php post_author_avatar($size='100'); ?>
                    <ul>
                        <li><div class="username"><?php post_author_link(); ?></div></li>
                        <li><?php post_author_title(); ?></span></li>
                    </ul>
                </div>
            </div>