<?php bb_get_header(); ?>

	<?php include_once('main-forums.php'); ?>
    <div id="content"><!-- Content -->
    	<div id="ajax">
    		<div id="ajaxInner">
        		<?php if ( $topics || $super_stickies ) : ?>  
        		<table id="forum-topics">
        			<thead>
            		<tr>
                		<th  class="title">آخرین گفت‌وگوها <?php if(bb_is_user_logged_in())	{ echo "&mdash; <a href='#post-form' class='jAdd'>افزودن »</a>"; } ?></th>
                		<th>نوشته‌ها</th>
                		<th>آخرین پاسخ</th>
                		<th>تازگی</th>
            		</tr>
            		</thead>
            		<tbody>
					<?php if ( $super_stickies ) : foreach ( $super_stickies as $topic ) : ?>
            		<tr<?php topic_class(); ?>>
                		<td class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                		<td><?php topic_posts(); ?></td>
                		<td><?php topic_last_poster(); ?></td>
                		<td><a href="<?php topic_last_post_link(); ?>"><?php topic_time(); ?></a></td>
            		</tr>
            		<?php endforeach; endif; ?>
					<?php if ( $topics ) : foreach ( $topics as $topic ) : ?>
            		<tr<?php topic_class(); ?>>
                		<td  class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                		<td><?php topic_posts(); ?></td>
                		<td><?php topic_last_poster(); ?></td>
                		<td><a href="<?php topic_last_post_link(); ?>"><?php topic_time(); ?></a></td>
            		</tr>
            		<?php endforeach; endif; ?>
            		</tbody>
        		</table>
        		<?php endif; // $topics ?>
			<div id="postPagination">
				<?php bb_latest_topics_pages(); ?><br><br>
			</div>
			</div>
		</div>
        <div id="post-form"><?php  if(bb_is_user_logged_in())	{ echo post_form();} ?></div>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery('#post-form').hide();
			});
			jQuery('.jAdd').click(function(){
				jQuery('#post-form').fadeIn(500);
			});
		</script>
    </div><!-- //c -->

<?php bb_get_footer(); ?>