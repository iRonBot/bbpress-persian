<?php bb_get_header(); ?>
        <div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » <a href="<?php forum_link(); ?>"><?php forum_name(); ?></a></div>
        <div id="fcontent">
			<?php if ( bb_forums( $forum_id ) ) : ?>
            <!-- Subforums -->
            <table id="sub-forums">
                <thead>
                <tr>
                    <th class="title">زیرانجمن‌ها</th>
                    <th>موضوع‌ها</th>
                    <th>نوشته‌ها</th>
                </tr>
                </thead>
                <tbody>
                <?php while ( bb_forum() ) : ?>
                <tr<?php bb_forum_class(); ?>>
                    <td class="title"><?php bb_forum_pad( '<div class="nest">' ); ?><a href="<?php forum_link(); ?>"><?php forum_name(); ?></a><small><?php forum_description( array( 'before' => '', 'after' => '' ) ); ?></small><?php bb_forum_pad( '</div>' ); ?></td>
                    <td><?php forum_topics(); ?></td>
                    <td><?php forum_posts(); ?></td>
                </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
            <?php endif; ?>
	    <div id="ajax">
	    	<div id="ajaxInner">
        <?php if ( $topics || $stickies ) : ?>
            <table id="forum-topics">
                <thead>
                <tr>
                    <th class="title">موضوع‌های <?php forum_name(); ?> <?php if(bb_get_forum_is_category() != bb_is_user_logged_in())	{ echo "&mdash; <a href='#fpost-form' class='jAdd'>افزودن »</a>"; } ?></th>
                    <th>نوشته‌ها</th>
                    <th>آخرین پاسخ</th>
                    <th>تازگی</th>
                </tr>
                </thead>
            <!-- Sticky -->
                <?php if ( $topics || $stickies ) : ?>
                <?php if ( $stickies ) : foreach ( $stickies as $topic ) : ?>
                <tr<?php topic_class(); ?>>
                    <td class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                    <td><?php topic_posts(); ?></td>
                    <td><?php topic_last_poster(); ?></td>
                    <td><a href="<?php topic_last_post_link(); ?>"><?php topic_time(); ?></a></td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                <?php if ( $topics ) : foreach ( $topics as $topic ) : ?>
                <tr<?php topic_class(); ?>>
                    <td class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                    <td><?php topic_posts(); ?></td>
                    <td><?php topic_last_poster(); ?></td>
                    <td><a href="<?php topic_last_post_link(); ?>"><?php topic_time(); ?></a></td>
                </tr>
                <?php endforeach; endif; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <?php endif; ?>
				<div id="postPagination">
		            <?php forum_pages(); ?>
				</div>
				</div>
	        </div>
            <div id="fpost-form"><?php if(bb_is_user_logged_in()) { echo post_form(); } ?></div>
			<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery('#fpost-form').hide();
			});
			jQuery('.jAdd').click(function(){
				jQuery('#fpost-form').fadeIn(500);
			});
			</script>
				</div>
<?php bb_get_footer(); ?>