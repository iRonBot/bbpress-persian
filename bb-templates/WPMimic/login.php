<?php bb_get_header(); ?>
<div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » ورود</div>
<div class="wrapper">
    <div id="forums">
        <div id="fcontent">
        <form method="post" action="<?php bb_option('uri'); ?>bb-login.php">
        <fieldset>
        <h2>ورود</h2>
        <table id="register">
        <?php if ( $user_exists ) : ?>
            <tr valign="top">
                <td scope="row"  class="title">شناسه:</td >
                <td ><input name="user_login" type="text" value="<?php echo $user_login; ?>" /></td >
            </tr>
            <tr valign="top" class="error">
                <td scope="row"  class="title">گذرواژه:</td >
                <td ><input name="password" type="password" /><br />
                <?php _e('گذرواژه نادرست است'); ?></td >
            </tr>
        <?php elseif ( isset($_POST['user_login']) ) : ?>
            <tr valign="top" class="error">
                <td scope="row" class="title">شناسه:</td >
                <td ><input name="user_login" type="text" value="<?php echo $user_login; ?>" /><br />
                <a href="<?php bb_option('uri'); ?>register.php?user=<?php echo $user_login; ?>"><?php _e('با این شناسه نام‌نویسی می‌کنید؟'); ?></a></td >
            </tr>
            <tr valign="top">
                <td scope="row"  class="title">گذرواژه:</td >
                <td ><input name="password" type="password" /></td >
            </tr>
        <?php else : ?>
            <tr valign="top" class="error">
                <td scope="row"  class="title">شناسه:</td >
                <td ><input name="user_login" type="text" /><br />
            </tr>
            <tr valign="top">
                <td scope="row"  class="title">گذرواژه:</td >
                <td ><input name="password" type="password" /></td >
            </tr>
        <?php endif; ?>
            <tr valign="top">
                <td scope="row"  class="title">مرا به خاطر بسپار</td >
                <td ><input name="remember" type="checkbox" id="remember" value="1"<?php echo $remember_checked; ?> /></td >
            </tr>
            <tr>
                <td scope="row"  class="title">&nbsp;</td >
                <td >
                    <input name="re" type="hidden" value="<?php echo $redirect_to; ?>" />
                    <input type="submit" value="<?php echo attribute_escape( isset($_POST['user_login']) ? __('تلاش دوباره &raquo;'): __('ورود &raquo;') ); ?>" />
                    <?php wp_referer_field(); ?>
                </td >
            </tr>
        </table>
        </fieldset>
        </form>
        <form method="post" action="<?php bb_uri('bb-reset-password.php', null, BB_URI_CONTEXT_FORM_ACTION + BB_URI_CONTEXT_BB_USER_FORMS); ?>">
        <fieldset>
        <h2>بازیابی گذرواژه</h2>
            <p><center>گذرواژه‌یتان را فراموش کرده‌اید؟ شاید بخواهید برای بازیابی دوباره‌ی آن، از فرم زیر استفاده کنید!</center></p>
        	<table id="recover-pass">
        		<tr valign="top">
        			<td scope="row" class="title">شناسه:</td>
        			<td><input name="user_login" id="user_login_reset_password" type="text" value="<?php echo $user_login; ?>" /></td>
            <tr>
                <td scope="row"  class="title">&nbsp;</td >
        		<td><input type="submit" value="<?php echo esc_attr__( 'بازیابی گذرواژه &raquo;' ); ?>" /></td>
        	</tr>
        	</table>
        </fielset>
        </form>
        </div>
    </div>
</div>
<?php bb_get_footer(); ?>