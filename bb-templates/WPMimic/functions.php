<?php
/* Functions for WPMimic theme */

// Custom Page Title
// Super simple way to rewrite custom page title...
if(!function_exists($bb_title))	{
function replace_title($bb_title){
	$separator = ' / ';
	
	if(in_array(bb_get_location(),array('front-page'))) {echo bb_option('name');}
	if(in_array(bb_get_location(),array('tag-page'))) {echo bb_option('name'); echo " &raquo; "; echo "برچسب‌ها";  echo " &raquo; "; echo bb_tag_name();}
	if(in_array(bb_get_location(),array('profile-page'))) {echo bb_option('name'); echo " &raquo; "; echo get_user_name($user->ID);}
    if(in_array(bb_get_location(),array('forum-page'))) {echo bb_option('name'); echo " &raquo; "; echo forum_name();}
	if(in_array(bb_get_location(),array('topic-page')))	{echo bb_option('name'); echo " &raquo; "; echo topic_title();}	
	if(in_array(bb_get_location(),array('register-page'))) {echo bb_option('name'); echo " &raquo; "; echo "نام‌نویسی";}
	if(in_array(bb_get_location(),array('login-page'))) {echo bb_option('name'); echo " &raquo; "; echo "ورود";}
	if(in_array(bb_get_location(),array('stats-page'))) {echo bb_option('name'); echo " &raquo; "; echo "آمار";}
	if(in_array(bb_get_location(),array('search-page'))) {echo bb_option('name'); echo " &raquo; "; echo "جست‌وجو";}
}
add_filter('bb_title', 'replace_title');
}

// The below actions are removed because we add our own checkbox
remove_action( 'post_form', 'bb_user_subscribe_checkbox' );
remove_action( 'edit_form', 'bb_user_subscribe_checkbox' );