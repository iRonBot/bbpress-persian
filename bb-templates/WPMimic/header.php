<?php
$_head_profile_attr = '';
if ( bb_is_profile() ) {
	global $self;
	if ( !$self ) {
		$_head_profile_attr = ' profile="http://www.w3.org/2006/03/hcard"';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"<?php bb_language_attributes( '1.1' ); ?>>
<head>
	<title><?php bb_title() ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="distribution" content="global" />
    <meta name="robots" content="follow, all" />
    
	<?php bb_feed_head(); ?>
    <link rel="stylesheet" href="<?php bb_option('uri'); ?>/bb-templates/WPMimic/resets.css" type="text/css" />
	<link rel="stylesheet" href="<?php bb_stylesheet_uri(); ?>" type="text/css" />    
	<?php if ( 'rtl' == bb_get_option( 'text_direction' ) ) : ?>
	<link rel="stylesheet" href="<?php bb_stylesheet_uri( 'rtl' ); ?>" type="text/css" />
	<?php endif; ?>

	<?php bb_head(); ?>
    <!--[if IE]>
    	<style type="text/css">
        	#header ul {margin-top:-8px;}
            #remember	{border: none; background: none;}
            #topics input,
            #topics textarea,
            #forums input,
            #forums textarea	{margin-bottom: 5px;}
            .post-user li.last-child	{border: none;}
		</style>        
    <![endif]-->
    <script type="text/javascript" charset="utf-8" src="<?php bb_uri(); ?>/bb-includes/js/jquery/jquery.js"></script>
	<script type="text/javascript" charset="utf-8">
		jQuery(document).ready(function(){

			jQuery('#postPagination a').live('click', function(e){
				e.preventDefault();
				var link = jQuery(this).attr('href');
				jQuery('#ajax').fadeOut(500).load(link + ' #ajaxInner', function(){ jQuery('#ajax').fadeIn(500); });

			});

		});
	</script>
</head>

<body id="<?php bb_location(); ?>">

<div class="wrapper header"><!-- Headerbar -->
	<div id="header">
    
    	<!-- Forum Title and Description -->
        <h1><a href="http://forums.ironbot.ir/"><?php bb_option('name'); ?></a>
        <span class="description"><?php if ( bb_get_option('description') ) : ?><?php bb_option('description'); ?><?php endif; ?></span></h1>
        <?php search_form(); ?>
        
        <!-- Menu Tab -->
        <?php include_once("menutab.php"); ?>
    </div>
</div><!-- //W-H -->

<div class="wrapper headline"><!-- Headerline -->
	<div id="headline">
        <h2>انجمن پشتیبانی</h2>
        <div class="hlright"><?php if ( !in_array( bb_get_location(), array( 'login-page', 'register-page' ) ) ) login_form(); ?></div>
    </div>
</div><!-- //W-U -->
<div id="forums"><!-- Forums -->