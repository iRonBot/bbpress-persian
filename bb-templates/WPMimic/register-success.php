<?php bb_get_header(); ?>
        <div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » پایان نام‌نویسی</div>
    	<div id="fcontent">
            <h2 id="register">عالی‌ست!</h2>
            <p><?php printf(__('نام‌نویسی شما با عنوان <strong>%s</strong> انجام شد. در چند دقیقه‌ی آینده گذرواژه برای‌تان پست الکترونیکی خواهد شد.'), $user_login) ?></p>
		</div>
<?php bb_get_footer(); ?>
