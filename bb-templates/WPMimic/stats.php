<?php bb_get_header(); ?>
        <div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » آمار</div>
    	<div id="sidebar">
        	<h2>آمار</h2>
            <dl>
                <dt>تعداد کابرها</dt>
                <dd><strong><?php total_users(); ?></strong></dd>
                <dt>تعداد نوشته‌ها</dt>
                <dd><strong><?php total_posts(); ?></strong></dd>
            </dl>
        </div>
        
        <div id="content">
			<?php if ($popular) : ?>
     		<table>
            	<thead>
                	<tr>
                    <th class="title">موضوع‌های پرطرفدار</th>
                    </tr>
                </thead>
                <tbody>
            	<?php foreach ($popular as $topic) : ?>
                <tr>
            	<td class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a> &#8212; <?php topic_posts(); ?> نوشته</td>
                </tr>
            	<?php endforeach; ?>
            	</tbody>
            </table><br>
            <?php endif; ?>
        
        <br></div>
<?php bb_get_footer(); ?>