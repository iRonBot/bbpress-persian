<?php bb_get_header(); ?>
<div class="bbcrumb"><span class="nav"><?php tag_pages(); ?></span><a href="<?php bb_option('uri'); ?>">انجمن</a> » <a href="<?php bb_option('uri'); ?>tags.php">برچسب‌ها</a> » <?php bb_tag_name(); ?></div>
<div class="wrapper">
    <div id="forums">
        <div id="sidebar">
            <h2>برچسب:  <?php bb_tag_name(); ?></h2>
            <div class="sbox">
                <ul>
                    <li><a href="<?php bb_tag_rss_link(); ?>" class="rss-link">پیوند خوراک RSS برای این برچسب</a></li>
                </ul>
                <?php manage_tags_forms(); ?>
            </div>
        </div>
        <div id="content">
            <?php do_action('tag_above_table', ''); ?>
            <?php if ( $topics ) : ?>
            <table>
            <thead>
            <tr>
                <th class="title">موضوع‌های مرتبط با <?php bb_tag_name(); ?> <?php if(bb_is_user_logged_in())	{ echo "&mdash; <a href='#post-form' class='jAdd'>افزودن »</a>"; } ?></th>
                <th>نوشته‌ها</th>
                <th>آخرین پاسخ</th>
                <th>تازگی</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ( $topics as $topic ) : ?>
            <tr<?php topic_class(); ?>>
                <td class="title"><?php bb_topic_labels(); ?> <a href="<?php topic_link(); ?>"><?php topic_title(); ?></a></td>
                <td><?php topic_posts(); ?></td>
                <td><?php topic_last_poster(); ?></td>
                <td><a href="<?php topic_last_post_link(); ?>"><?php topic_time(); ?></a></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
            <?php endif; ?>
            <div id="post-form"><?php post_form(); ?></div>
			<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function(){
				jQuery('#post-form').hide();
			});
			jQuery('.jAdd').click(function(){
				jQuery('#post-form').fadeIn(500);
			});
			</script>
            <?php do_action('tag_below_table', ''); ?>
    	</div>
	</div>
</div>
<?php bb_get_footer(); ?>