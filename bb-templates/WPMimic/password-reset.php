<?php bb_get_header(); ?>
<div class="bbcrumb"><a href="<?php bb_option('uri'); ?>">انجمن</a> » بازیابی گذرواژه</div>
<div class="wrapper">
	<div id="forums">
    	<div id="fcontent">
			<?php if ( $reset ) : ?>
            <p>گذرواژه‌یتان بازسازی و به رایانامه‌یتان فرستاده شد.</p>
            <?php else : ?>
            <center>رایانامه‌ای به نشانی که داده بودید فرستاده شده‌است. اگر آن را در طول چند دقیقه دریافت نکردید و یا این‌که نشانی رایانامه‌تان تغییر پیدا کرده‌است
            <br/><br/><div class="lcu">با ما تماس بگیرید!</div></center>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php bb_get_footer(); ?>
