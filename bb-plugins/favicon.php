<?php
  /*
  Plugin Name: فـاوآیکون بی‌بی‌پرس
  Description: به مدیر انجمن بی‌بی‌پرس این امکان را می‌دهد تا برای انجمن خود فـاوآیکون شخصی‌سازی کند.
  Plugin URI: https://bitbucket.org/iRonBot/bbpress-persian
  Author: فرشاد حسینی
  Author URI: http://ironbot.ir
  Version: 0.0.1
  */
  function bb_sitewide_favicon()
  {
    echo '<link rel="shortcut icon" href="http://ironbot.ir/wp-content/themes/polished/images/favicon.ico" />';
  }
  add_action('bb_admin_head', 'bb_sitewide_favicon');
  add_action('bb_head', 'bb_sitewide_favicon');
?>