<?php
  /*
  Plugin Name: بارگذاری تصویر بی‌بی‌پرس
  Description: با این افزونه می‌توانید در مطلب‌های خود تصویر دلخواه خود را درج کنید و انجمن بی‌بی‌پرس قادر به نمایش آن است.
  Plugin URI:  https://bitbucket.org/iRonBot/bbpress-persian
  Author: فرشاد حسینی
  Author URI: http://ironbot.ir
  Version: 0.0.9
  */
  function allow_images_allowed_tags($tags)
  {
	  $tags['img'] = array('src' => array(), 'title' => array(), 'alt' => array());
	  return $tags;
  }
  add_filter( 'bb_allowed_tags', 'allow_images_allowed_tags' );
?>